'use strict';

const Handlebars = require('handlebars');
const moment = require('moment');


Handlebars.registerHelper('createdAt', function (time) {
    return moment(time).format('YYYY-MM-DD, hh:mm A');
});

Handlebars.registerHelper('descriptionShow', function (description) {
    try {
        let a= document.write(description)
        return a;
    } catch (error) {
        console.log(error)
    }
});

Handlebars.registerHelper('textTrimmerhandle', function (text, trimSize) {
    if (text === '') {
        return 'No Description';
    }
    try {
        if (text.length > trimSize) {
            return text.substring(0, trimSize) + '...';
        } else {
            return text.substring(0, trimSize);
        }
    } catch (error) {
        return text;
    }

});