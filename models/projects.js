const mongoose = require('mongoose');

const projectSchema = new mongoose.Schema({
    project_name: { type: String },
    phone: {
        type: String
    },
    userType: {
        type: String,
    },
    height: {
        type: String
    },
    width: {
        type: String
    },
    materials: {
        type: String
    },
    quantity: {
        type: String
    },
    designTime: {
        type: String
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    },
    bill: {
        type: String
    }
});



const Project = mongoose.model('Project', projectSchema);

module.exports = Project;
