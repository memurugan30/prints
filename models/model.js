const mongoose = require('mongoose');

const modelSchema = new mongoose.Schema({
    model_name: { type: String },
    make_id: {
        type: String
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    coverPic: {
        type: String,
        default: ' '
    },
    description: {
        type: String,
        default: ' '
    },
    updatedAt: {
        type: Date,
        default: Date.now
    },
});



const Model = mongoose.model('Model', modelSchema);

module.exports = Model;
