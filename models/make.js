const mongoose = require('mongoose');

const makeSchema = new mongoose.Schema({
  make_name: { type: String },
  year_name: {
    type: String
  },
  assets: {
    type: String,
    default: ' '
  },
  coverPic: {
    type: String,
    default: ' '
  },
  description: {
    type: String,
    default: ' '
  },
  flag: {
    type: Boolean,
    default: false
  },
  showImaged: {
    type: Boolean,
    default: false
  },
  make_id: {
    type: String
  },
  createdAt: {
    type: Date,
      default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  },
});



const Make = mongoose.model('Make', makeSchema);

module.exports = Make;
