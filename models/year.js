const mongoose = require('mongoose');

const yearSchema = new mongoose.Schema({
  year_name: { type: String, unique: true },
  createdAt: {
    type: Date,
      default: Date.now
  },
  updatedAt: {
    type: Date,
    default: Date.now
  },
});



const Year = mongoose.model('Year', yearSchema);

module.exports = Year
