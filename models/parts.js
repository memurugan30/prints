const mongoose = require('mongoose');

const partSchema = new mongoose.Schema({
    part_name: { type: String },
    coverPic: {
        type: String,
        default: ' '
    },
    description: {
        type: String,
        default: ' '
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        default: Date.now
    },
});



const Parts = mongoose.model('Parts', partSchema);

module.exports = Parts;
