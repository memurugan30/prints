var yearJson = require('../config/year.json');
var yearHelper = require('../helpers/year');

exports.index = async (req, res) => {
    if (req.session.user && req.cookies.user_id) {
        var years = await yearHelper.getAllYears();
        if(years.statusCode === 200) {
            res.render('year/index', {
                title: 'Year Management', years: years.data
            });
        } else {
            res.render('year/index', {
                title: 'Year Management'
            });
        }
    } else {
        res.redirect('/login');
    }
};

exports.yearJson = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            var bulkImport = await yearHelper.bulkJsonImport(yearJson);
            if (bulkImport.statusCode === 200) {
                res.status(200).json({
                    statusCode: 200,
                });
            } else {
                res.status(200).json({
                    statusCode: 500,
                });
            }
    
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        console.log(error)
    }
};


exports.addYears = async (req, res) => {
    if (req.session.user && req.cookies.user_id) {
        res.render('year/add', {
            title: 'Add Years', type: 'Add Years'
        });
    } else {
        res.redirect('/login');
    }
};


exports.postYears = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            var yearJson = [];
            req.body.dynamic_form.dynamic_form[0].map(item => {
                yearJson.push({ 'year_name': item })
            });

            var multipleImport = await yearHelper.multipleImport(yearJson);
            if (multipleImport.statusCode === 200) {
                res.redirect('/year');
            } else {
                res.redirect('/year');
            }
    
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        console.log(error)
    }
};

exports.multipleDelete = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            var multipleDelete = await yearHelper.multipleDelete(req.body);
            if (multipleDelete.statusCode === 200) {
                res.redirect('/year');
            } else {
                res.redirect('/year');
            }
    
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        console.log(error)
    }
};

exports.editYear = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            let checkYear = await yearHelper.findYearById(req.params.id);
            if(checkYear.statusCode === 200) {
                res.render('year/edit', {
                    title: 'Edit Year', type: 'Edit Years', year: checkYear.data
                });
            } else {
                res.redirect('/year');
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        return '';
    }
};

exports.updateYears = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            var update = await yearHelper.updateYear(req.body.year_name, req.body.uid);
            if (update.statusCode === 200) {
                res.redirect('/year');
            } else {
                res.redirect('/year');
            }
    
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        console.log(error)
    }
};

exports.deleteYear = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            let deleteYear = await yearHelper.deleteYearById(req.params.id);
            if(deleteYear.statusCode === 200) {
                res.redirect('/year');
            } else {
                res.redirect('/year');
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        return '';
    }
};
