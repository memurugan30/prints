var modelJson = require('../config/model.json');
var modelHelper = require('../helpers/model');
var yearHelper = require('../helpers/year');
var makeHelper = require('../helpers/make');

exports.index = async (req, res) => {
    if (req.session.user && req.cookies.user_id) {
        var years = await yearHelper.getAllYears();
        if (years.statusCode) {
            res.render('model/select', {
                title: 'Home', years: years.data
            });
        } else {
            res.render('model/select', {
                title: 'Home'
            });
        }
    } else {
        res.redirect('/login');
    }
};

exports.modelJson = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            var bulkImport = await modelHelper.bulkJsonImport(modelJson);
            if (bulkImport.statusCode === 200) {
                res.status(200).json({
                    statusCode: 200,
                });
            } else {
                res.status(200).json({
                    statusCode: 500,
                });
            }

        } else {
            res.redirect('/login');
        }
    } catch (error) {
        console.log(error)
    }
};

exports.getMakeByYear = async (req, res) => {
    if (req.session.user && req.cookies.user_id) {
        var makes = await makeHelper.getMakesBy(req.params.year);
        if (makes.statusCode === 200) {
            res.render('partials/modelManagment/makeSelect', {
                makes: makes.data, layout: false
            });
        } else {
            res.render('model/select', {
            });
        }
    } else {
        res.redirect('/login');
    }
};

exports.getModelByMake = async (req, res) => {
    if (req.session.user && req.cookies.user_id) {
        var getMake = await makeHelper.findMakeByMakeId(req.params.make);
        var models = await modelHelper.getModelByMakeId(req.params.make);
        if (models.statusCode === 200) {
            res.render('model/index', {
                title: 'Model Management', models: models.data, make: getMake.data, make_id: req.params.make
            });
        } else {
            res.render('model/select', {
            });
        }
    } else {
        res.redirect('/login');
    }
};

exports.addModels = async (req, res) => {
    if (req.session.user && req.cookies.user_id) {
        var getMake = await makeHelper.findMakeByMakeId(req.params.make);
        if (getMake.statusCode === 200) {
            res.render('model/add', {
                title: 'Add Models', type: 'Add Models', make: getMake.data,
            });
        } else {
            res.render('model/add', {
                title: 'Add Models', type: 'Add Models'
            });
        }
    } else {
        res.redirect('/login');
    }
};

exports.postModels = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            var modelJson = [];
            req.body.dynamic_form_model.dynamic_form_model[0].map(item => {
                modelJson.push({ 'make_id': req.body.make, 'model_name': item  })
            });

            var multipleImport = await modelHelper.bulkJsonImport(modelJson);
            if (multipleImport.statusCode === 200) {
                res.redirect('/model/' + req.body.make);
            } else {
                res.redirect('/model/' + req.body.make);
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        console.log(error)
    }
};

exports.editModel = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            let checkModel = await modelHelper.findModelById(req.params.id);
            if (checkModel.statusCode === 200) {
                res.render('model/edit', {
                    title: 'Edit Model', type: 'Edit Model', model: checkModel.data
                });
            } else {
                res.redirect('/model/select');
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        return '';
    }
};

exports.updateModel = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            var update = await modelHelper.updateModel(req.body.model_name, req.body.uid);
            if (update.statusCode === 200) {
                res.redirect('/model/' + update.data.make_id);
            } else {
                res.redirect('/model/select');
            }

        } else {
            res.redirect('/login');
        }
    } catch (error) {
        console.log(error)
    }
};


exports.deleteModel = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            let checkModel = await modelHelper.findModelById(req.params.id);
            if (checkModel.statusCode === 200) {
                let deleteModel = await modelHelper.deleteModelById(req.params.id);
                if (deleteModel.statusCode === 200) {
                    res.redirect('/model/' + checkModel.data.make_id);
                } else {
                    res.redirect('/model/' + checkModel.data.make_id);
                }
            } else {
                res.redirect('/model/select');
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        return '';
    }
};

exports.uploadCover = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            let checkOld = await modelHelper.findModelByModel(req.params.model);
            if (checkOld.statusCode === 200) {
                let updateCover = await modelHelper.updateModelCover(req.params.model, '/uploads/'+req.file.filename);
                if (updateCover.statusCode === 200) {
                    res.redirect('/model/'+req.params.make);
                } else {
                    res.redirect('/model/'+req.params.make);
                }
            } else {
                res.redirect('/model/'+req.params.make);
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        return '';
    }
};

exports.modelDescription = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            let checkModel = await modelHelper.findModelById(req.params.id);
            if (checkModel.statusCode === 200) {
                res.render('model/description', {
                    title: 'Model Description', type: 'Description', model: checkModel.data
                });
            } else {
                res.redirect('/model/'+checkModel.data.make_id);
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        return '';
    }
};

exports.updateModelDescription = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            var update = await modelHelper.updateModelDescription(req.body.description, req.body.uid);
            if (update.statusCode === 200) {
                res.redirect('/model/'+update.data.make_id);
            } else {
                res.redirect('/model/'+update.data.make_id);
            }

        } else {
            res.redirect('/login');
        }
    } catch (error) {
        console.log(error)
    }
};