const Joi = require('joi');
const userHelpers = require('../helpers/user');

exports.getLogin = (req, res) => {
  if (req.user) {
    return res.redirect('/');
  }
  res.render('login/index', {
    title: 'Login'
  });
};

exports.postLogin = async (req, res) => {
  var validCheck = Joi.object().keys({
    email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    password: Joi.string().min(5).max(20).required(),
  });
  const result = Joi.validate(req.body, validCheck);
  const { value, error } = result;
  const valid = error == null;
  if (!valid) {
    res.status(422).json({
      message: 'Invalid request'
    });
  } else {
    try {
      var loginUser = await userHelpers.checkLogin(req.body.email, req.body.password);
      if(loginUser.statusCode === 200) {
        req.session.user = loginUser.data;
        res.redirect('/dashboard');
      } else {
        res.redirect('/login');
      }
      // User.findOne({ where: { username: username } }).then(function (user) {
      //   if (!user) {
      //     res.redirect('/login');
      //   } else if (!user.validPassword(password)) {
      //     res.redirect('/login');
      //   } else {
      //     req.session.user = user.dataValues;
      //     res.redirect('/dashboard');
      //   }
      // });
    } catch (error) {
      console.log('sdsd')
    }
  }
};

exports.postSignup = async (req, res) => {
  var validCheck = Joi.object().keys({
    name: Joi.string().min(3).max(30).required(),
    email: Joi.string().email({ minDomainAtoms: 2 }).required(),
    password: Joi.string().min(5).max(20).required(),
  });
  const result = Joi.validate(req.body, validCheck);
  const { value, error } = result;
  const valid = error == null;
  if (!valid) {
    res.status(422).json({
      message: 'Invalid request'
    });
  } else {
    try {
      var user = {
        email: req.body.email,
        name: req.body.name,
        password: req.body.password,
      }
      let signup = await userHelpers.register(user);
      if (signup.statusCode === 200) {
        res.status(201).json({ message: 'successfully registered' });
      } else {
        res.status(500).json({ message: 'internal error' });
      }
    } catch (error) {
      console.log(error)
    }
  }
};
