var partJson = require('../config/part.json');
const partHelper = require('../helpers/parts'); 

exports.index = async (req, res) => {
    if (req.session.user && req.cookies.user_id) {
        var parts = await partHelper.getAllParts();
        if(parts.statusCode === 200) {
            res.render('parts/index', {
                title: 'Parts', parts: parts.data
            });
        } else {
            res.render('parts/index', {
                title: 'Parts'
            });
        }
    } else {
        res.redirect('/login');
    }
};

exports.partJson = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            var bulkImport = await partHelper.bulkJsonImport(partJson);
            if (bulkImport.statusCode === 200) {
                res.status(200).json({
                    statusCode: 200,
                });
            } else {
                res.status(200).json({
                    statusCode: 500,
                });
            }
    
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        console.log(error)
    }
};


exports.addParts = async (req, res) => {
    if (req.session.user && req.cookies.user_id) {
        res.render('parts/add', {
            title: 'Add Parts', type: 'Add Parts'
        });
    } else {
        res.redirect('/login');
    }
};


exports.postParts = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            var partsJson = [];
            req.body.dynamic_form_parts.dynamic_form_parts[0].map(item => {
                partsJson.push({ 'part_name': item })
            });

            var multipleImport = await partHelper.bulkJsonImport(partsJson);
            if (multipleImport.statusCode === 200) {
                res.redirect('/parts');
            } else {
                res.redirect('/parts');
            }
    
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        console.log(error)
    }
};

exports.editPart = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            let checkPart = await partHelper.findPartById(req.params.id);
            if(checkPart.statusCode === 200) {
                res.render('parts/edit', {
                    title: 'Edit Part', type: 'Edit Part', part: checkPart.data
                });
            } else {
                res.redirect('/parts');
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        return '';
    }
};

exports.updatePart = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            var update = await partHelper.updatePart(req.body.part_name, req.body.uid);
            if (update.statusCode === 200) {
                res.redirect('/parts');
            } else {
                res.redirect('/parts');
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        console.log(error)
    }
};

exports.deletePart = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            let deletePart = await partHelper.deletePartById(req.params.id);
            if(deletePart.statusCode === 200) {
                res.redirect('/parts');
            } else {
                res.redirect('/parts');
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        return '';
    }
};

exports.uploadCover = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            let checkOld = await partHelper.findPartById(req.params.part);
            if (checkOld.statusCode === 200) {
                let updateCover = await partHelper.updatePartCover(req.params.part, '/uploads/'+req.file.filename);
                if (updateCover.statusCode === 200) {
                    res.redirect('/parts');
                } else {
                    res.redirect('/parts');
                }
            } else {
                res.redirect('/parts');
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        return '';
    }
};

exports.partDescription = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            let checkPart = await partHelper.findPartById(req.params.id);
            if (checkPart.statusCode === 200) {
                res.render('parts/description', {
                    title: 'Part Description', type: 'Description', part: checkPart.data
                });
            } else {
                res.redirect('/parts');
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        return '';
    }
};

exports.updatePartDescription = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            var update = await partHelper.updatePartDescription(req.body.description, req.body.uid);
            if (update.statusCode === 200) {
                res.redirect('/parts');
            } else {
                res.redirect('/parts');
            }

        } else {
            res.redirect('/login');
        }
    } catch (error) {
        console.log(error)
    }
};