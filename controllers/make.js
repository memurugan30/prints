var makeJson = require('../config/make.json');
var makeHelper = require('../helpers/make');
var yearHelper = require('../helpers/year');
var fs = require('fs');

exports.index = async (req, res) => {
    if (req.session.user && req.cookies.user_id) {
        var years = await yearHelper.getAllYears();
        var makes = await makeHelper.getMakesUnique();
        if (years.statusCode === 200 && makes.statusCode === 200) {
            res.render('make/select', {
                title: 'Make Management', years: years.data, makes: makes.data
            });
        } else {
            res.render('make/select', {
                title: 'Make Management'
            });
        }
    } else {
        res.redirect('/login');
    }
};

exports.makeManagement = async (req, res) => {
    if (req.session.user && req.cookies.user_id) {
        var makes = await makeHelper.getMakesBy(req.params.type);
        if (makes.statusCode === 200 && makes.statusCode === 200) {
            res.render('make/index', {
                title: 'Make Management', makes: makes.data, year_name: req.params.type
            });
        } else {
            res.render('make/index', {
                title: 'Make Management'
            });
        }
    } else {
        res.redirect('/login');
    }
};

exports.makeJson = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            var bulkImport = await makeHelper.bulkJsonImport(makeJson);
            if (bulkImport.statusCode === 200) {
                res.status(200).json({
                    statusCode: 200,
                });
            } else {
                res.status(200).json({
                    statusCode: 500,
                });
            }

        } else {
            res.redirect('/login');
        }
    } catch (error) {
        console.log(error)
    }
};



exports.addMakes = async (req, res) => {
    if (req.session.user && req.cookies.user_id) {
        var years = await yearHelper.getAllYears();
        if (years.statusCode === 200) {
            res.render('make/add', {
                title: 'Add Makes', type: 'Add Makes', years: years.data, year_name: req.params.year
            });
        } else {
            res.render('make/add', {
                title: 'Add Makes', type: 'Add Makes'
            });
        }
    } else {
        res.redirect('/login');
    }
};


exports.postMakes = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            let getMakeIdCount = await makeHelper.makeLastCount();
            if (getMakeIdCount.statusCode === 200) {
                var makeJson = [];
                var counter = 0;
                req.body.dynamic_form_make.dynamic_form_make[0].map(item => {
                    counter += 1;
                    makeJson.push({ 'year_name': req.body.year_name, 'make_name': item, 'make_id': parseInt(getMakeIdCount.data) + parseInt(counter) })
                });

                var multipleImport = await makeHelper.bulkJsonImport(makeJson);
                if (multipleImport.statusCode === 200) {
                    res.redirect('/make/' + req.body.year_name);
                } else {
                    res.redirect('/make/' + req.body.year_name);
                }

            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        console.log(error)
    }
};

exports.editMake = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            let checkMake = await makeHelper.findMakeById(req.params.id);
            if (checkMake.statusCode === 200) {
                res.render('make/edit', {
                    title: 'Edit Make', type: 'Edit Make', make: checkMake.data
                });
            } else {
                res.redirect('/make/select');
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        return '';
    }
};

exports.updateMake = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            var update = await makeHelper.updateMake(req.body.make_name, req.body.uid);
            if (update.statusCode === 200) {
                res.redirect('/make/' + update.data.year_name);
            } else {
                res.redirect('/year');
            }

        } else {
            res.redirect('/login');
        }
    } catch (error) {
        console.log(error)
    }
};


exports.deleteMake = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            let checkMake = await makeHelper.findMakeById(req.params.id);
            if (checkMake.statusCode === 200) {
                let deleteMake = await makeHelper.deleteMakeById(req.params.id);
                if (deleteMake.statusCode === 200) {
                    res.redirect('/make/' + checkMake.data.year_name);
                } else {
                    res.redirect('/make/' + checkMake.data.year_name);
                }
            } else {
                res.redirect('/make/select');
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        return '';
    }
};

exports.lookManagement = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            let getUniqueMakes = await makeHelper.getMakesUniqueWithAll();
            if (getUniqueMakes.statusCode === 200) {
                res.render('make/look', {
                    title: 'Make Look Management', makes: getUniqueMakes.data
                });
            } else {
                res.render('make/look', {
                    title: 'Make Look Management',
                });
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        return '';
    }
};

exports.uploadLogo = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            let checkOld = await makeHelper.findMakeByMake(req.params.make);
            if (checkOld.statusCode === 200) {
                let updateAssets = await makeHelper.updateMakeAssets(req.params.make, '/uploads/'+req.file.filename);
                if (updateAssets.statusCode === 200) {
                    res.redirect('/make/look');
                } else {
                    res.redirect('/make/look');
                }
            } else {
                res.redirect('/make/look');
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        return '';
    }
};

exports.uploadCover = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            let checkOld = await makeHelper.findMakeByMake(req.params.make);
            if (checkOld.statusCode === 200) {
                let updateCover = await makeHelper.updateMakeCover(req.params.make, '/uploads/'+req.file.filename);
                if (updateCover.statusCode === 200) {
                    res.redirect('/make/look');
                } else {
                    res.redirect('/make/look');
                }
            } else {
                res.redirect('/make/look');
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        return '';
    }
};

exports.makeDescription = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            let checkMake = await makeHelper.findMakeById(req.params.id);
            if (checkMake.statusCode === 200) {
                res.render('make/description', {
                    title: 'Make Description', type: 'Description', make: checkMake.data
                });
            } else {
                res.redirect('/make/select');
            }
        } else {
            res.redirect('/login');
        }
    } catch (error) {
        return '';
    }
};

exports.updateMakeDescription = async (req, res) => {
    try {
        if (req.session.user && req.cookies.user_id) {
            var update = await makeHelper.updateMakeDescription(req.body.description, req.body.uid);
            if (update.statusCode === 200) {
                res.redirect('/make/look');
            } else {
                res.redirect('/make/look');
            }

        } else {
            res.redirect('/login');
        }
    } catch (error) {
        console.log(error)
    }
};