/**
 * Module dependencies.
 */
const express = require('express');
const compression = require('compression');
const session = require('express-session');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const chalk = require('chalk');
const errorHandler = require('errorhandler');
const lusca = require('lusca');
const dotenv = require('dotenv');
const MongoStore = require('connect-mongo')(session);
const flash = require('express-flash');
const path = require('path');
const mongoose = require('mongoose');
// const expressStatusMonitor = require('express-status-monitor');
const sass = require('node-sass-middleware');
const multer = require('multer');
var exphbs = require('express-handlebars');

// const upload = multer({ dest: path.join(__dirname, 'uploads') });
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads')
  },
  filename: function (req, file, cb) {
    cb(null, req.params.namer)
  }
})
 
var upload = multer({ storage: storage })

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
dotenv.config({ path: '.env' });

/**
 * Controllers (route handlers).
 */
const homeController = require('./controllers/home');
const userController = require('./controllers/user');

/**
 * Create Express server.
 */
const app = express();

/**
 * Connect to MongoDB.
 */
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
mongoose.connect(process.env.MONGODB_URI);
mongoose.connection.on('error', (err) => {
  console.error(err);
  console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
  process.exit();
});

/**
 * Express configuration.
 */
// app.set('host', process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0');
app.set('port', process.env.PORT || 8090);
app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'pug');
var handlebars = exphbs.create({
  layoutsDir: path.join(__dirname, "views"),
  partialsDir: path.join(__dirname, "views/partials"),
  defaultLayout: 'main',
  extname: 'hbs',
  helpers: require('./config/handlebar-helpers')
});

app.engine('hbs', handlebars.engine);
app.set('view engine', 'hbs');
// app.use(expressStatusMonitor());
app.use(compression());
app.use(sass({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public')
}));

app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
  key: 'user_id',
  // secret: 'keyjoin',
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  cookie: { maxAge: 1209600000 }, // two weeks in milliseconds
  store: new MongoStore({
    url: process.env.MONGODB_URI,
    autoReconnect: true,
  })
}));
// app.use((req, res, next) => {
//   if (req.cookie.user_id && !req.session.user) {
//     res.clearCookie('user_id');
//   }
//   next();
// });
app.use(flash());

app.use(lusca.xframe('SAMEORIGIN'));
app.use(lusca.xssProtection(true));
app.disable('x-powered-by');
// app.use((req, res, next) => {
//   res.locals.user = req.user;
//   next();
// });
app.use('/', express.static(path.join(__dirname, 'public')));
// app.use('/', express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }));

// app.use(logger("[:date[iso]] Started :method :url for :remote-addr"));
// app.use(logger("[:date[iso]] Completed :status :res[content-length] in :response-time ms"));

// app.use((req, res, next) => {
//   if (req.cookies.user_sid && !req.session.user) {
//       res.clearCookie('user_sid');        
//   }
//   next();
// });

// middleware function to check for logged-in users
// var sessionChecker = (req, res, next) => {
//   if (req.session.user && req.cookies.user_id) {
//     res.redirect('/dashboard');
//   } else {
//     next();
//   }
// };
// app.get('/', sessionChecker, (req, res) => {
//   res.redirect('/login');
// });
/**
 * Primary app routes.
 */

app.get('/admin', homeController.index);
app.get('/designer', homeController.designer);
app.get('/designer/add/project', homeController.addProjects);
app.post('/add-projects', homeController.postProjects);
// app.route('/login')
//     .get( userController.getLogin)
//     .post(userController.postLogin);

app.get('/logout', (req, res) => {
  if (req.session.user && req.cookies.user_id) {
      res.clearCookie('user_id');
      res.redirect('/');
  } else {
      res.redirect('/login');
  }
});
/**
 * Error Handler.
 */
if (process.env.NODE_ENV === 'development') {
  // only use in development
  app.use(errorHandler());
} else {
  app.use((err, req, res, next) => {
    console.error(err);
    res.status(500).send('Server Error');
  });
}

/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
  console.log('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env'));
  console.log('  Press CTRL-C to stop\n');
});

module.exports = app;
