$(function () {

    'use strict'

    $('.importJsonModel').on('click', function () {
        $.ajax({
            url: '/model/json',
            type: 'GET',
            success: function (response) {
                window.location.reload();
            },
            error: function (error) {
            }
        });
    });

    $('#modelDataTable').DataTable();
    var table = $('#modelDataTable').DataTable();
    $("#filter").on('change', function() {
        table.columns([1,2]).search($(this).val()).draw();
    }); 


    $('.getMakeByModel').on('change', function () {
        var year = $(this).val();
        $.ajax({
            url: '/model/select/'+year,
            type: 'GET',
            success: function (response) {
                $('.makeSelectBox').html('');
                $('.makeSelectBox').append(response);
            },
            error: function (error) {
            }
        });
    });

    $(document).on('change', '.makeSelectBox', function(){
        $('.getModelBtn').attr('disabled',false)
    });

    $(document).on('click', '.getModelBtn', function(){
        if($('.makeSelectBox').val() !== null || $('.makeSelectBox').val() !== undefined || $('.makeSelectBox').val() !== '') {
            window.location = 'model/'+$('.makeSelectBox').val() ;
        }
    });

    var dynamic_form = $("#dynamic_form_model").dynamicForm("#dynamic_form_model", "#plusModel", "#minusModel", {
        limit: 100,
        formPrefix: "dynamic_form_model",
        normalizeFullForm: false
    });
    $("#dynamic_form_model #minusModel").on('click', function () {
        var initDynamicId = $(this).closest('#dynamic_form_model').parent().find("[id^='dynamic_form_model']").length;
        if (initDynamicId === 2) {
            $(this).closest('#dynamic_form_model').next().find('#minusModel').hide();
        }
        $(this).closest('#dynamic_form_model').remove();
    });

    $(document).on('click', '.deleteModel', function() {
        if(confirm("Are you sure you want to delete this?")){
            window.location = "/model/delete/"+$(this).attr('uid');
        }
        else{
            return false;
        }
    });

    $('.model-cover').on('click', function() {
        let model_name = $(this).attr('model');
        let make = $(this).attr('make');
        $('.coverUploadFormModel').attr('action','/model/upload/cover/'+model_name+'/'+'model-cover-'+model_name+'/'+make);
        $('.show-model-cover').modal('show');
    });

    $('.textareaModel').summernote();
});