$(function () {

    'use strict'

    $('.importJsonParts').on('click', function () {
        $.ajax({
            url: '/part/json',
            type: 'GET',
            success: function (response) {
                window.location.reload();
            },
            error: function (error) {
            }
        });/*  */
    });

    $('#partsDataTable').DataTable();
    var table = $('#partsDataTable').DataTable();
    $("#filter").on('change', function() {
        table.columns([1,2]).search($(this).val()).draw();
    }); 

    var dynamic_form = $("#dynamic_form_parts").dynamicForm("#dynamic_form_parts", "#plusParts", "#minusParts", {
        limit: 100,
        formPrefix: "dynamic_form_parts",
        normalizeFullForm: false
    });

    // dynamic_form.inject([{p_name: 'Hemant',quantity: '123',remarks: 'testing remark'},{p_name: 'Harshal',quantity: '123',remarks: 'testing remark'}]);

    $("#dynamic_form_parts #minusParts").on('click', function () {
        var initDynamicId = $(this).closest('#dynamic_form_parts').parent().find("[id^='dynamic_form_parts']").length;
        if (initDynamicId === 2) {
            $(this).closest('#dynamic_form_parts').next().find('#minusParts').hide();
        }
        $(this).closest('#dynamic_form_parts').remove();
    });

    

    $(document).on('click', '.deletePart', function() {
        if(confirm("Are you sure you want to delete this?")){
            window.location = "/part/delete/"+$(this).attr('uid');
        }
        else{
            return false;
        }
    });

    $('.part-cover').on('click', function() {
        let part = $(this).attr('part');
        let part_name = $(this).attr('part_name');
        $('.coverUploadFormPart').attr('action','/part/upload/cover/'+part+'/'+'part-cover-'+part_name);
        $('.show-part-cover').modal('show');
    });

    $('.textareaPart').summernote()

});