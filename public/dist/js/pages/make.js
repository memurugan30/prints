$(function () {

    'use strict'

    $('.importJsonMake').on('click', function () {
        $.ajax({
            url: '/make/json',
            type: 'GET',
            success: function (response) {
                window.location.reload();
            },
            error: function (error) {
            }
        });/*  */
    });

    $('#makeDataTable').DataTable();
    var table = $('#makeDataTable').DataTable();
    $("#filter").on('change', function() {
        table.columns([1,2]).search($(this).val()).draw();
    }); 

    var dynamic_form = $("#dynamic_form_make").dynamicForm("#dynamic_form_make", "#plusMake", "#minusMake", {
        limit: 100,
        formPrefix: "dynamic_form_make",
        normalizeFullForm: false
    });
    $("#dynamic_form_make #minusMake").on('click', function () {
        var initDynamicId = $(this).closest('#dynamic_form_make').parent().find("[id^='dynamic_form_make']").length;
        if (initDynamicId === 2) {
            $(this).closest('#dynamic_form_make').next().find('#minusMake').hide();
        }
        $(this).closest('#dynamic_form_make').remove();
    });


    $(document).on('click', '.deleteMake', function() {
        if(confirm("Are you sure you want to delete this?")){
            window.location = "/make/delete/"+$(this).attr('uid');
        }
        else{
            return false;
        }
    });

    $('#makeLookDataTable').DataTable();
    var tableMake = $('#makeLookDataTable').DataTable();
    $("#filter").on('change', function() {
        tableMake.columns([1,2]).search($(this).val()).draw();
    }); 

    $('.make-logo').on('click', function() {
        let make_name = $(this).attr('make');
        $('.logoUploadForm').attr('action','/make/upload/logo/'+make_name+'/'+'make-logo-'+make_name);
        $('.show-make-logo').modal('show');
    });

    $('.make-cover').on('click', function() {
        let make_name = $(this).attr('make');
        $('.coverUploadForm').attr('action','/make/upload/cover/'+make_name+'/'+'make-cover-'+make_name);
        $('.show-make-cover').modal('show');
    });

    $('.textareaMake').summernote()
    
});