$(function () {

    'use strict'

    $('.importJsonYear').on('click', function () {
        $.ajax({
            url: '/year/json',
            type: 'GET',
            success: function (response) {
                window.location.reload();
            },
            error: function (error) {
            }
        });/*  */
    });

    $('#yearDataTable').DataTable({
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        "paging": true,
        "lengthChange": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "searching": true,
        "select": {
            "style": 'multi'
        }
    });

    var dynamic_form = $("#dynamic_form").dynamicForm("#dynamic_form", "#plus5", "#minus5", {
        limit: 100,
        formPrefix: "dynamic_form",
        normalizeFullForm: false
    });

    // dynamic_form.inject([{p_name: 'Hemant',quantity: '123',remarks: 'testing remark'},{p_name: 'Harshal',quantity: '123',remarks: 'testing remark'}]);

    $("#dynamic_form #minus5").on('click', function () {
        var initDynamicId = $(this).closest('#dynamic_form').parent().find("[id^='dynamic_form']").length;
        if (initDynamicId === 2) {
            $(this).closest('#dynamic_form').next().find('#minus5').hide();
        }
        $(this).closest('#dynamic_form').remove();
    });

    var checkedArray = [];
    $('.multiYear').on('change', function () {
        if ($(this).is(':checked')) {
            checkedArray.push($(this).attr('uid'));
        } else {
            checkedArray.splice($.inArray($(this).attr('uid'), checkedArray), 1);
        }
    });

    $('.multipleDelete').on('click', function (e) {
        e.preventDefault();
        var r = confirm("Are you sure?");
        if (r == true) {
            var data = [];
            for (var i = 0; i < checkedArray.length; i++) {
                data.append('formData[]', checkedArray[i]);
            }
            $.ajax({
                url: '/year/multidelete',
                type: 'POST',
                data: data,
                success: function (response) {
                },
                error: function (error) {
                }
            });
        }
    });


    $(document).on('click', '.deleteYear', function() {
        if(confirm("Are you sure you want to delete this?")){
            window.location = "/year/delete/"+$(this).attr('uid');
        }
        else{
            return false;
        }
    });

});