const Project = require('../models/projects');

exports.checkLastBill = async () => {
    return new Promise(async function (resolve, reject) {
        try {
            var bill = await Project.find().skip(Project.count() - 1);
            if (bill.length !== 0) {
                return resolve({
                    statusCode: 200,
                    data: parseInt(bill.bill) + 1
                });
            } else {
                return resolve({
                    statusCode: 200,
                    data: 1
                });
            }
        } catch (error) {
            return ''
        }
    });
}