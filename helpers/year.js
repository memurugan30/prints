const Year = require('../models/year');

exports.bulkJsonImport = async (year) => {
    return new Promise(async function (resolve, reject) {
        try {
            var save = await Year.insertMany(year);
            if (save) {
                return resolve({
                    statusCode: 200,
                    data: save
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.getAllYears = async () => {
    return new Promise(async function (resolve, reject) {
        try {
            var years = await Year.find();
            if (years) {
                return resolve({
                    statusCode: 200,
                    data: years
                });
            } else {
                return resolve({
                    statusCode: 404
                }); 
            }
        } catch (error) {
            return ''
        }
    });
}

exports.multipleImport = async (year) => {
    return new Promise(async function (resolve, reject) {
        try {
            var save = await Year.insertMany(year);
            if (save) {
                return resolve({
                    statusCode: 200,
                    data: save
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.multipleDelete = async (year) => {
    return new Promise(async function (resolve, reject) {
        try {
            var deleteAll = await Year.deleteMany({ _id : { $in : year } });
            if (deleteAll) {
                return resolve({
                    statusCode: 200
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.findYearById = async (id) => {
    return new Promise(async function (resolve, reject) {
        try {
            var year = await Year.findOne({ _id: id });
            if (year) {
                return resolve({
                    statusCode: 200,
                    data: year
                });
            } else {
                return resolve({
                    statusCode: 404
                }); 
            }
        } catch (error) {
            return ''
        }
    });
}

exports.updateYear = async (year_name, uid) => {
    return new Promise(async function(resolve, reject){
      try {
        var update = await Year.findOneAndUpdate({ _id: uid }, 
          {$set:{
            year_name: year_name
          }}, { returnOriginal: false });
        if(update) {
          return resolve({
            statusCode : 200,
            data: update
          });
        }
      } catch (error) {
        
      }
    });
  }

  exports.deleteYearById = async (id) => {
    return new Promise(async function (resolve, reject) {
        try {
            var year = await Year.findOneAndDelete({ _id: id });
            if (year) {
                return resolve({
                    statusCode: 200
                });
            } else {
                return resolve({
                    statusCode: 404
                }); 
            }
        } catch (error) {
            return ''
        }
    });
}