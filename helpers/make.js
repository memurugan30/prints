const Make = require('../models/make');

exports.bulkJsonImport = async (make) => {
    return new Promise(async function (resolve, reject) {
        try {
            var save = await Make.insertMany(make);
            if (save) {
                return resolve({
                    statusCode: 200,
                    data: save
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.getAllmakes = async () => {
    return new Promise(async function (resolve, reject) {
        try {
            var make = await Make.find();
            if (make) {
                return resolve({
                    statusCode: 200,
                    data: make
                });
            } else {
                return resolve({
                    statusCode: 404
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.getMakesBy = async (type) => {
    return new Promise(async function (resolve, reject) {
        try {
            var make = await Make.find({ $or: [{ year_name: type }, { make_name: type }] });
            if (make) {
                return resolve({
                    statusCode: 200,
                    data: make
                });
            } else {
                return resolve({
                    statusCode: 404
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.getMakesUnique = async () => {
    return new Promise(async function (resolve, reject) {
        try {
            var make = await Make.find().distinct('make_name');
            var data = [];
            make.map(item => {
                data.push({ 'make_name': item })
            });
            if (make) {
                return resolve({
                    statusCode: 200,
                    data: data
                });
            } else {
                return resolve({
                    statusCode: 404
                });
            }
        } catch (error) {
            return ''
        }
    });
}


exports.findMakeById = async (id) => {
    return new Promise(async function (resolve, reject) {
        try {
            var make = await Make.findOne({ _id: id });
            if (make) {
                return resolve({
                    statusCode: 200,
                    data: make
                });
            } else {
                return resolve({
                    statusCode: 404
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.updateMake = async (make_name, uid) => {
    return new Promise(async function (resolve, reject) {
        try {
            var update = await Make.findOneAndUpdate({ _id: uid },
                {
                    $set: {
                        make_name: make_name
                    }
                }, { returnOriginal: false });
            if (update) {
                return resolve({
                    statusCode: 200,
                    data: update
                });
            }
        } catch (error) {

        }
    });
}

exports.deleteMakeById = async (id) => {
    return new Promise(async function (resolve, reject) {
        try {
            var make = await Make.findOneAndDelete({ _id: id });
            if (make) {
                return resolve({
                    statusCode: 200
                });
            } else {
                return resolve({
                    statusCode: 404
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.findMakeByMakeId = async (id) => {
    return new Promise(async function (resolve, reject) {
        try {
            var make = await Make.findOne({ make_id: id });
            if (make) {
                return resolve({
                    statusCode: 200,
                    data: make
                });
            } else {
                return resolve({
                    statusCode: 404
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.makeLastCount = async () => {
    return new Promise(async function (resolve, reject) {
        try {
            var skipper = await Make.count() - 1;
            var make = await Make.findOne().skip(skipper);
            if (make) {
                return resolve({
                    statusCode: 200,
                    data: make.make_id
                });
            } else {
                return resolve({
                    statusCode: 404
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.getMakesUniqueWithAll = async () => {
    return new Promise(async function (resolve, reject) {
        try {
            var make = await Make.find();
            var result = make.reduce((unique, o) => {
                if (!unique.some(obj => obj.make_name === o.make_name)) {
                    unique.push(o);
                }
                return unique;
            }, []);
            if (result) {
                return resolve({
                    statusCode: 200,
                    data: result
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.findMakeByMake = async (make_name) => {
    return new Promise(async function (resolve, reject) {
        try {
            var make = await Make.findOne({ make_name: make_name });
            if (make) {
                return resolve({
                    statusCode: 200,
                    data: make
                });
            } else {
                return resolve({
                    statusCode: 404
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.updateMakeAssets = async (make_name, assetLoc) => {
    return new Promise(async function (resolve, reject) {
        try {
            var update = await Make.updateMany({ make_name: make_name },
                {
                    $set: {
                        assets: assetLoc
                    }
                }, { returnOriginal: false });
            if (update) {
                return resolve({
                    statusCode: 200,
                    data: update
                });
            }
        } catch (error) {

        }
    });
}

exports.updateMakeCover = async (make_name, assetLoc) => {
    return new Promise(async function (resolve, reject) {
        try {
            var update = await Make.updateMany({ make_name: make_name },
                {
                    $set: {
                        coverPic: assetLoc
                    }
                }, { returnOriginal: false });
            if (update) {
                return resolve({
                    statusCode: 200,
                    data: update
                });
            }
        } catch (error) {

        }
    });
}

exports.updateMakeDescription = async (description, uid) => {
    return new Promise(async function (resolve, reject) {
        try {
            var update = await Make.findOneAndUpdate({ _id: uid },
                {
                    $set: {
                        description: description
                    }
                }, { returnOriginal: false });
            if (update) {
                return resolve({
                    statusCode: 200,
                    data: update
                });
            }
        } catch (error) {

        }
    });
}