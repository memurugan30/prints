const Parts = require('../models/parts');

exports.bulkJsonImport = async (parts) => {
    return new Promise(async function (resolve, reject) {
        try {
            var save = await Parts.insertMany(parts);
            if (save) {
                return resolve({
                    statusCode: 200,
                    data: save
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.getAllParts = async () => {
    return new Promise(async function (resolve, reject) {
        try {
            var parts = await Parts.find();
            if (parts) {
                return resolve({
                    statusCode: 200,
                    data: parts
                });
            } else {
                return resolve({
                    statusCode: 404
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.findPartById = async (id) => {
    return new Promise(async function (resolve, reject) {
        try {
            var part = await Parts.findOne({ _id: id });
            if (part) {
                return resolve({
                    statusCode: 200,
                    data: part
                });
            } else {
                return resolve({
                    statusCode: 404
                }); 
            }
        } catch (error) {
            return ''
        }
    });
}

exports.updatePart = async (part_name, uid) => {
    return new Promise(async function(resolve, reject){
      try {
        var update = await Parts.findOneAndUpdate({ _id: uid }, 
          {$set:{
            part_name: part_name
          }}, { returnOriginal: true });
        if(update) {
          return resolve({
            statusCode : 200,
            data: update
          });
        }
      } catch (error) {
        
      }
    });
  }

  exports.deletePartById = async (id) => {
    return new Promise(async function (resolve, reject) {
        try {
            var part = await Parts.findOneAndDelete({ _id: id });
            if (part) {
                return resolve({
                    statusCode: 200
                });
            } else {
                return resolve({
                    statusCode: 404
                }); 
            }
        } catch (error) {
            return ''
        }
    });
}

exports.updatePartCover = async (partId, assetLoc) => {
    return new Promise(async function (resolve, reject) {
        try {
            var update = await Parts.updateMany({ _id: partId },
                {
                    $set: {
                        coverPic: assetLoc
                    }
                }, { returnOriginal: false });
            if (update) {
                return resolve({
                    statusCode: 200,
                    data: update
                });
            }
        } catch (error) {

        }
    });
}

exports.updatePartDescription = async (description, uid) => {
    return new Promise(async function (resolve, reject) {
        try {
            var update = await Parts.findOneAndUpdate({ _id: uid },
                {
                    $set: {
                        description: description
                    }
                }, { returnOriginal: false });
            if (update) {
                return resolve({
                    statusCode: 200,
                    data: update
                });
            }
        } catch (error) {

        }
    });
}