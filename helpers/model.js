const Model = require('../models/model');

exports.bulkJsonImport = async (model) => {
    return new Promise(async function (resolve, reject) {
        try {
            var save = await Model.insertMany(model);
            if (save) {
                return resolve({
                    statusCode: 200,
                    data: save
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.getModelByMakeId = async (id) => {
    return new Promise(async function (resolve, reject) {
        try {
            var model = await Model.find({ make_id: id });
            if (model) {
                return resolve({
                    statusCode: 200,
                    data: model
                });
            } else {
                return resolve({
                    statusCode: 404
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.findModelById = async (id) => {
    return new Promise(async function (resolve, reject) {
        try {
            var model = await Model.findOne({ _id: id });
            if (model) {
                return resolve({
                    statusCode: 200,
                    data: model
                });
            } else {
                return resolve({
                    statusCode: 404
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.updateModel = async (model_name, uid) => {
    return new Promise(async function (resolve, reject) {
        try {
            var update = await Model.findOneAndUpdate({ _id: uid },
                {
                    $set: {
                        model_name: model_name
                    }
                }, { returnOriginal: true });
            if (update) {
                return resolve({
                    statusCode: 200,
                    data: update
                });
            }
        } catch (error) {

        }
    });
}

exports.deleteModelById = async (id) => {
    return new Promise(async function (resolve, reject) {
        try {
            var models = await Model.findOneAndDelete({ _id: id });
            if (models) {
                return resolve({
                    statusCode: 200
                });
            } else {
                return resolve({
                    statusCode: 404
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.findModelByModel = async (model_name) => {
    return new Promise(async function (resolve, reject) {
        try {
            var model = await Model.findOne({ model_name: model_name });
            if (model) {
                return resolve({
                    statusCode: 200,
                    data: model
                });
            } else {
                return resolve({
                    statusCode: 404
                });
            }
        } catch (error) {
            return ''
        }
    });
}


exports.updateModelCover = async (model_name, assetLoc) => {
    return new Promise(async function (resolve, reject) {
        try {
            var update = await Model.updateMany({ model_name: model_name },
                {
                    $set: {
                        coverPic: assetLoc
                    }
                }, { returnOriginal: false });
            if (update) {
                return resolve({
                    statusCode: 200,
                    data: update
                });
            }
        } catch (error) {

        }
    });
}

exports.updateModelDescription = async (description, uid) => {
    return new Promise(async function (resolve, reject) {
        try {
            var update = await Model.findOneAndUpdate({ _id: uid },
                {
                    $set: {
                        description: description
                    }
                }, { returnOriginal: false });
            if (update) {
                return resolve({
                    statusCode: 200,
                    data: update
                });
            }
        } catch (error) {

        }
    });
}