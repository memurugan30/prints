const User = require('../models/user');
const bcrypt = require('bcrypt');

exports.register = async (user) => {
    return new Promise(async function (resolve, reject) {
        var newUser = new User(user);
        try {
            var save = await newUser.save();
            if (save) {
                return resolve({
                    statusCode: 200,
                    data: save
                });
            }
        } catch (error) {
            return ''
        }
    });
}

exports.checkLogin = async (email, password) => {
    return new Promise(async function (resolve, reject) {
        try {
            var emailCheck = await User.findOne({ email: email });
            if (emailCheck) {
                var passwordsMatch = await bcrypt.compare(password, emailCheck.password);
                if(passwordsMatch) {
                    return resolve({
                        statusCode: 200,
                        data: emailCheck
                    });
                } else {
                    return resolve({
                        statusCode: 404
                    });
                }
            } else {
                return resolve({
                    statusCode: 404
                });
            }
        } catch (error) {
            return ''
        }
    });
}